<?php

class Usuario
{
    //Atributos
    private $id;
    private $nome;
    private $email;
    private $senha;


    public function __construct()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "aulaphp";
        
        // Create connection
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
            echo 'errado';
            return;
        } else {
            echo 'Conectado <br>';
        }

    }

    public function setcodigo(int $id)
    {
        return $this->id = $id;

    }

    public function setnome(string $nome)
    {
        return $this->nome = $nome;

    }

    public function setemail(string $email)
    {
        return $this->email = $email;

    }

    public function setsenha(string $senha)
    {
        return $this->senha = $senha;

    }

    public function getId(int $id) : int
    {
        return $this->id = $id;

    }

    public function getnome(string $nome) : string
    {
        return $this->$nome;

    }

    public function getemail(string $email) : string
    {
        return $this->$email;

    }

    public function getsenha(string $senha) : string
    {
        return $this->$senha;

    }
    public function enviar()
    {
        $senha = password_hash($this->senha, PASSWORD_DEFAULT);
        $objStmt = $this->conn->prepare('REPLACE INTO USUARIOS (CODIGO, NOME, EMAIL, SENHA)   VALUES (?,?,?,?);');
        //isere os dados para fazer a consulta
        $objStmt->bind_param('isss', $this->codigo, $this->nome, $this->email, $senha);
        if ($objStmt->execute()) {
            echo 'Dados salvos';
        } else {
            echo 'Erro ao salvar os dados';
        }
    }

    public function listar()
    /* Metodo listar todos*/
    {
        $listar = "select codigo,nome,email,senha from usuarios";
        $result = $this->conn->query($listar);


        echo "
<table border=1>
    <thead>
        <th>
            Codigo
        </th>
        <th>
            Nome
        </th>
        <th>
            Email 
        </th>
        <th>
            Senha 
        </th>
    
    </thead>";
        $cont = 0;
        while ($retorno = $result->fetch_assoc()) {
            $c_linha = $cont % 2;

            $cor = $c_linha == 0 ? 'white' : 'gray';
            echo "<tr bgcolor = $cor> 
        <td>
            {$retorno['codigo']}
        </td>
        <td>
            {$retorno['nome']}
        </td>
        <td>
            {$retorno['email']}
        </td>
        <td>
            {$retorno['senha']}
        </td>
    </tr>";
            $cont++;

        };
        echo
            "</table>";
    }
    
    /*
    {
        Listar usuario específico

        $listar = $this->conn->prepare('SELECT USUARIOS (CODIGO, NOME, EMAIL, SENHA) WHERE CODIGO = ?');
        //isere os dados para fazer a consulta
        $listar->bind_param('i', $this->codigo);
        $listar->execute();
        $listar = $listar->get_result();
        $listar->fetch_assoc();
        
           
    }
    */

    public function deletar(int $id)
    {
        $objStmt = $this->conn->prepare('DELETE FROM USUARIOS WHERE CODIGO = ?');
        $objStmt->bind_param('i', $this->id);
        if ($objStmt->execute()) {
            echo 'Dados apagados com sucesso';
        } else {
            echo 'Erro ao apagar os dados';
        }
    }

    public function _destruct()
    {
        mysqli_close($this->conn);
    }
}


