<?php

class template{
    private $arquivo;
    private $vetor;

    public function setArquivo( string $arquivo) : boll {
        $this->arquivo = $arquivo;
    }

    public function setVariaveis( array $vetor) : boll {
        $this->variaveis = $vetor;
    }

    public function carregar(){
        include ($this->arquivo);
    }
}