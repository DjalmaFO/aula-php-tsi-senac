<?php

$dsn = 'mysql:dbname=aulaphp;host=127.0.0.1;port=3308';
$user = 'root';
$password = '';

try {
    $conex = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

$codigo = 3;

$query = "select * from usuarios where codigo = :codigo";
$prep = $conex->prepare($query);
$prep->execute(array(':codigo' => $codigo));


$saida = $prep->fetch(PDO::FETCH_ASSOC);


/* Consultar erro
echo "\nPDOStatement::errorInfo():\n";
$erro = $prep->errorInfo();
print_r($erro);
*/

var_dump($saida);