<?php
echo '<pre>';

// Constante no PHP
define('qtd_paginas', 10);

echo "Valor da minha constante é ". qtd_paginas;

//variavel para assar valor  a uma constante
$ip_do_banco = '192.168.45.12';

define('ip_do_banco', $ip_do_banco);

echo "\nO IP do SGDB é " . ip_do_banco;

//Constantes magicas
echo "\nestou na inha". __LINE__;

echo "\neste é o arquivo". __FILE__;

//muito bom para depurar o codigo

echo "\n\n";

var_dump($ip_do_banco);

// nunca esquecer do ";" cacete!

//vetores - haja coração XD

echo "forma 1";
$dias_da_semana = [
    "dom",
    "seg",
    "ter",
    "qua",
    "qui",
    "sex",
    "sab"
    ];

unset($dias_da_semana); // destroi a variavel

echo "\n";
var_dump($dias_da_semana);

$dias_da_semana[0] = "Dom";
$dias_da_semana[1] = "Seg";
$dias_da_semana[2] = "Ter";
$dias_da_semana[3] = "Qua";
$dias_da_semana[4] = "Qui";
$dias_da_semana[5] = "Sex";
$dias_da_semana[6] = "Sab";

echo "\n";
var_dump($dias_da_semana);

$dias_da_semana = array(0 => "Dom",
                        1 => "Seg",
                        2 => "Ter",
                        3 => "Qua",
                        4 => "Qui",
                        5 => "Sex",
                        6 => "Sab",

);
echo "\n";
var_dump($dias_da_semana);



echo'<\pre>';


