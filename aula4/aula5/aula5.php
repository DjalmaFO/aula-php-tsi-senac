<?php
$alunos = array(
    0 => array( 'nome' => 'Djalma','bitbucket' => 'https://pagina_djalma'),
    1 => array('nome' => 'Emerson','bitbucket' => 'https://pagina_emerson'),
    2 => array('nome' => 'Sergio','bitbucket' => 'https://pagina_sergio'),
    3 => array('nome' => 'Bruno','bitbucket' => 'https://pagina_bruno'),
    4 => array('nome' => 'Beatriz','bitbucket' => 'https://pagina_beatriz')

);

// Operador Ternário
echo"
<table border=1>
    <thead>
        <th>
            Nome
        </th>
        <th>
            Bitbucket 
        </th>
    
    </thead>";
$cont = 0;
while ($cont < count($alunos)){
    $c_linha = $cont % 2;

    $cor = $c_linha == 0 ? 'white' : 'gray';
    echo "<tr bgcolor = $cor> 
        <td>
            {$alunos[$cont]['nome']}
        </td>
        <td>
            {$alunos[$cont]['bitbucket']}
        </td>
    </tr>";
    $cont++;

};
echo
"</table>";


// Usando if

echo"
<table border=1>
    <thead>
        <th>
            Nome
        </th>
        <th>
            Bitbucket 
        </th>
    
    </thead>";
$cont = 0;
while ($cont < count($alunos)){
    $c_linha = $cont % 2;

    if($c_linha == 0){
        $cor = 'blue';
     }
     
    else{
        $cor = 'pink';
    };
    echo "<tr bgcolor = $cor> 
        <td>
            {$alunos[$cont]['nome']}
        </td>
        <td>
            {$alunos[$cont]['bitbucket']}
        </td>
    </tr>";
    $cont++;

};
echo
"</table>";

//Boleano

$nome = 'Curtinho';

$posicao = strpos($nome, 'nho');

if($posicao !== false){
    echo"Encontardo na posição $posicao!";
};

